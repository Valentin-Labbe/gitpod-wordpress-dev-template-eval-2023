<?php

function load_styles(){
    wp_enqueue_style("twentynineteen-style", get_template_directory_uri()."/style.css");
    wp_enqueue_style("child-style", get_stylesheet_directory_uri()."/style.css");
}
add_action("wp_enqueue_scripts", "load_styles");

function wporg_custom_post_type() {
	register_post_type('wporg_recipe',
		array(
			'labels'      => array(
				'name'          => __( 'Recipes', 'textdomain' ),
				'singular_name' => __( 'Recipe', 'textdomain' ),
			),
			'public'      => true,
			'has_archive' => true,
            'show_in_rest' => true,
			'rewrite'     => array( 'slug' => 'recipe' ), // my custom slug
		)
	);
}
add_action('init', 'wporg_custom_post_type');

function wporg_create_ingredient_textarea($post) {
    //$value = get_post_meta( $post->ID, '_wporg_meta_key', true );
    ?>
            <label for="wporg_field">Ingrédients :</label>

            <textarea id="wporg_field" name="wporg_field" rows="5" cols="33"></textarea>
	<?php
}

function wporg_add_ingredient_box() {
	$screens = [ 'post', 'wporg_cpt' ];
	foreach ( $screens as $screen ) {
		add_meta_box(
			'wporg_ingredient_id',                 // Unique ID
			'Infos complémentaires',      // Box title
			'wporg_create_ingredient_textarea',  // Content callback, must be of type callable
			$screen                            // Post type
		);
	}
}
add_action( 'add_meta_boxes', 'wporg_add_ingredient_box' );

function wporg_save_postdata( $post_id ) {
	if ( array_key_exists( 'wporg_field', $_POST ) ) {
		update_post_meta(
			$post_id,
			'_wporg_meta_key',
			$_POST['wporg_field']
		);
	}
}
add_action( 'save_post', 'wporg_save_postdata' );